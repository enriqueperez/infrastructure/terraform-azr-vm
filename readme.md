## Training: deploy and provision VM instance on Azure subscription

Basic deployment and provision of a vm into subscription. Custom configuration of vm
can be found in `terraform.tfvars` which must be updated.

For this example we are using `Ubuntuserver 16.04.0-LTS` image distribution and some packages are installed with `remote-exec` terraform ***provisioner***. For other distros, update accordingly in main.tf.

##### Prerequisites
- Azure subscription. (You can use free tier sub).
- Azure cli installed and configured
- logged to azure sub: `az login`

#### Step 1 - Init plugins
```bash
terraform init
```
Azure plugin version: `~>2.16.0`
#### Step 2 - Set the parameters
Terraform will automatically read default vars from `terraform.tfvars`. Update
the values in here first.
```
subscription_id = "replace_me"
location        = "replace_me"
admin_password  = "replace_me"
admin_username  = "replace_me"
```
#### Step 3 - Show the plan
```bash
terraform plan
```
#### Step 4 - Create resource
```bash
terraform apply -auto-approve
```

***

#### Destroy resources
The resources will have the costs. To destroy all of them, run the following.
```bash
terraform destroy -auto-approve
```

> `Important:` Just be careful!, if you have removed the terraform.tfstate file created when resources were deployed, then you won't be able to destroy using this command, and this will be only possible manually in the UI portal or through az cli.
