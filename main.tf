# Configure in here the provider plugin.
provider "azurerm" {
  version         = "~>2.16.0"
  subscription_id = var.subscription_id
  features {}
}

# Following configs are implicit dependency. Which means azure and terraform already
# knows which resources must be created in which order

# Create new resource group
resource "azurerm_resource_group" "enperez-rg" {
  name     = "enperez-rg"
  location = var.location

  tags = {
    Environment = "Terraform training"
    Team        = "DevOps"
  }
}

# network resource.
resource "azurerm_virtual_network" "enperez-vnet" {
  name                = "enperez-vnet"
  address_space       = ["10.0.0.0/16"]
  location            = var.location
  resource_group_name = azurerm_resource_group.enperez-rg.name

  tags = {
    Environment = "Terraform training"
    Team        = "DevOps"
  }
}

# the subnet
resource "azurerm_subnet" "enperez-subnet" {
  name                 = "enperez-subnet"
  resource_group_name  = azurerm_resource_group.enperez-rg.name
  virtual_network_name = azurerm_virtual_network.enperez-vnet.name
  address_prefixes     = ["10.0.2.0/24"]
}

# public ip
resource "azurerm_public_ip" "enperez-publicip" {
  name                = "enperez-publicip"
  location            = var.location
  resource_group_name = azurerm_resource_group.enperez-rg.name
  allocation_method   = "Static"

  tags = {
    Environment = "Terraform training"
    Team        = "DevOps"
  }
}

# get data of public ip created for the output.
data "azurerm_public_ip" "ip" {
  name                = azurerm_public_ip.enperez-publicip.name
  resource_group_name = azurerm_virtual_machine.enperez-vm.resource_group_name
}

# network security group
resource "azurerm_network_security_group" "enperez-nsg" {
  name                = "enperez-nsg"
  location            = var.location
  resource_group_name = azurerm_resource_group.enperez-rg.name

  security_rule {
    name                       = "SSH"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags = {
    Environment = "Terraform training"
    Team        = "DevOps"
  }
}

# network interface
resource "azurerm_network_interface" "enperez-nic" {
  name                = "enperez-nic"
  location            = var.location
  resource_group_name = azurerm_resource_group.enperez-rg.name

  ip_configuration {
    name                          = "enperez-nic-ip"
    subnet_id                     = azurerm_subnet.enperez-subnet.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.enperez-publicip.id
  }

  tags = {
    Environment = "Terraform training"
    Team        = "DevOps"
  }
}

# the vm
resource "azurerm_virtual_machine" "enperez-vm" {
  name                  = "enperez-vm"
  location              = var.location
  resource_group_name   = azurerm_resource_group.enperez-rg.name
  network_interface_ids = [azurerm_network_interface.enperez-nic.id]
  vm_size               = "Standard_B1ms"

  storage_os_disk {
    name              = "enperez-vm-osdisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04.0-LTS"
    version   = "latest"
  }

  os_profile {
    computer_name  = "enperez-vm-training"
    admin_username = var.admin_username
    admin_password = var.admin_password
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  provisioner "remote-exec" {
    connection {
      host     = azurerm_public_ip.enperez-publicip.ip_address
      type     = "ssh"
      user     = var.admin_username
      password = var.admin_password
    }

    inline = [
      "cat /etc/os-release",
      "sudo apt-get update",
      "sudo apt -y install pwgen htop vim git"
    ]
  }

  tags = {
    Environment = "Terraform training"
    Team        = "DevOps"
  }

}

# outputs
# ----- [PUBLICIP]----------
output "instance-public-ip" {
  value = azurerm_public_ip.enperez-publicip.ip_address
}
